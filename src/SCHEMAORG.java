
import java.util.List;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

public class SCHEMAORG {
	//mapping the vhdl.owl URI's
	public static String ONTOLOGY_URL = "http://schema.org/";
	//classes
	public static Resource GeoShape 				= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "GeoShape");
	public static Resource GeoCoordinates 			= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "GeoCoordinates");
	
	//object properties
	public static Property isPartOf				 	= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "isPartOf");
	public static Property hasPart				 	= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "hasPart");
	public static Property geo				 		= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "geo");
		
	//data properties
	public static Property latitude 				= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "latitude");
	public static Property longitude 				= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "longitude");
	public static Property name		 				= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "name");
	
	public static String mapTypeToOntologyType(String type) {
		// get component types from the ontology
		List<String> architectureTypes = Virtuoso.getSubClasses("#Node");

		for (String s : architectureTypes) {
			if (type.toLowerCase().contains(s.toLowerCase())
					|| s.toLowerCase().contains(type.toLowerCase())) {
				return SCHEMAORG.ONTOLOGY_URL + s;
			}
		}

		return null;
	}
	
}