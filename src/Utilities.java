
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.UUID;

import com.eu.miscedautils.vhdlparser.FVhPEntity;

public class Utilities {
	
	public static String generateSystemTimeId() {
		return UUID.randomUUID().toString();
	}
	
	public static String getClassName(String resourceURL) {
		if(resourceURL != null){
			return resourceURL.substring(resourceURL.lastIndexOf('#') + 1);
		} else {
			return null;
		}
	}
	
	public static String getAbsoluteURL(String domainURL, String relativeURL){
		if(relativeURL != null){
			return domainURL + getClassName(relativeURL);
		} else {
			return null;
		}
	}
	
	public static boolean isArchive(String fileName) {
		return
		fileName.substring(fileName.length()-3,fileName.length()).toLowerCase().equals("zip")
		||
		fileName.substring(fileName.length()-3,fileName.length()).toLowerCase().equals("rar")
		||
		fileName.substring(fileName.length()-3,fileName.length()).toLowerCase().equals("tar")
		||
		fileName.substring(fileName.length()-6,fileName.length()).toLowerCase().equals("tar.gz");
	}
	
	
	public static boolean isVhdlFile(String fileName) {
		return fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase()
				.equals("vhd")
				|| fileName.substring(fileName.lastIndexOf('.') + 1)
						.toLowerCase().equals("vhdl");
	}
	
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		// The directory is now empty so delete it
		return dir.delete();
	}
	
	public static String genRandomTimeId() {
		return UUID.randomUUID().toString();//.replace("-", "");
	}

	public static ArrayList<String> getListsIntersection(ArrayList<String> a,
			ArrayList<String> b) {
		ArrayList<String> result = new ArrayList<String>();

		if (a.size() == 0 || b.size() == 0) {
			// return empty array
			return result;
		}

		for (String s : a) {
			for (String t : b) {
				if (s.equals(t) && !result.contains(s)) {
					result.add(s);
				}
			}
		}

		return result;
	}
	
	public static ArrayList<String> filterLongerThan3(ArrayList<String> list){
		ArrayList<String> result = new ArrayList<String>();
		for (String s : list) {
			if (s.length() >= 3) {
				result.add(s);
				result.add(s);
			}
		}
		return result;
	}
	
	public static boolean checkWord(String varName){
		ArrayList<String> invalidChars = new ArrayList<String>();
		invalidChars.add("<");
		invalidChars.add(">");
		invalidChars.add("/");
		invalidChars.add("\\");
		invalidChars.add(":");
		invalidChars.add("@");
		invalidChars.add(".");
		invalidChars.add("-");
		invalidChars.add(",");
		invalidChars.add("!");
		invalidChars.add("__");  //two underscores is invalid, one is allowed
		invalidChars.add("#");
		
		if(varName.length()<3){
			return false;
		}
		for (String c : invalidChars) {
			if(varName.contains(c)){
				return false;
			}
		}
		
		return true;
	}
	
	public static ArrayList<String> getTokensFromName(String name) {
		ArrayList<String> result = new ArrayList<String>();

		StringTokenizer st = new StringTokenizer(name, "_0123456789");
		
		String token = null;
		while (st.hasMoreTokens()) {
			token = st.nextToken();
			if(checkWord(token)){
				result.add(token);
			}
		}

		return result;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}