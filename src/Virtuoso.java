import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.sparql.vocabulary.DOAP;
import com.hp.hpl.jena.vocabulary.DCTerms;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

//import org.apache.log4j.Logger;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

public class Virtuoso {
	// log
	// static Logger log = Logger.getLogger(ModelAccess.class.getName());
	
	public static final Integer MAX_RESULT_LIST_SIZE = 10;

	public static final String VIRTUOSO_URL = "jdbc:virtuoso://localhost:1111/";
	public static final String UNAME = "dba";
	public static final String PASS = "coresiphdl";
	public static final String GRAPH = DATAREPO.ONTOLOGY_URL;

//	public static final String ONTOLOGY_SERVICE_ENDPOINT = "http://linkeddata.finki.ukim.mk/sparql";
	
//	public static final String ONTOLOGY_SERVICE_ENDPOINT = "http://hdlipcores.finki.ukim.mk/sparql";
	
	public static final String ONTOLOGY_SERVICE_ENDPOINT =  "http://localhost:8890/sparql";
	
	// select remote or local data repo
	// remote data repo
//	 public static final String DATAREPO_SERVICE_ENDPOINT = "http://hdlipcores.finki.ukim.mk/sparql";
	// local data repo
	public static final String DATAREPO_SERVICE_ENDPOINT = "http://localhost:8890/sparql";

	private static VirtGraph set;
	
	public static String SPARQL_PREFIXES = 
			  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
			+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
			+ "PREFIX doap: <http://usefulinc.com/ns/doap#>\n"
			+ "PREFIX dct: <http://purl.org/dc/terms/>\n"
			+ "PREFIX pgo: <http://purl.org/net/hdlipcores/ontology/pgo#>\n"
			+ "PREFIX s: <http://schema.org/>\n";

	public static VirtGraph getVirtuosoGraph() {
		if (set == null) {
			set = new VirtGraph(GRAPH, VIRTUOSO_URL, UNAME, PASS);
		}
		return set;
	}

	public static boolean addTripleToGlobalModel(Triple t) {
		if (set == null || set.isClosed()) {
			getVirtuosoGraph();
		}

		try {
			if (!set.contains(t)) {
				set.add(t);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static ArrayList<String> getSubClasses(String parentName) {
		// result list
		ArrayList<String> childList = new ArrayList<String>();

		// map properties
		String sparqlString = SPARQL_PREFIXES
				+ "SELECT ?a \n"
				+ "FROM <"
				+ PGO.ONTOLOGY_URL
				+ "> \n"
				+ "WHERE { \n"
				+ "?a rdfs:subClassOf "+"<"+parentName+">"
				+ " \n}";
		

		// Create model
		String service = ONTOLOGY_SERVICE_ENDPOINT;
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, sparqlString);

		try {
			ResultSet results = qe.execSelect();

			for (; results.hasNext();) {
				QuerySolution soln = results.nextSolution();
				RDFNode a = soln.get("a");
				childList.add(a.asNode().getLocalName());
			}
		} finally {
			qe.close();
		}

		return childList;
	}
	
	public static void deleteTriplesFromRepository(String modelUrl) {

		String url;
		url = "jdbc:virtuoso://localhost:1111";

		/* STEP 1 */
		VirtGraph set = new VirtGraph("test1", url, "dba", "coresiphdl");

		/* STEP 3 */
		/* Select all data in virtuoso */

		Query sparql = QueryFactory.create("SELECT ?s FROM <" + modelUrl
				+ "> WHERE {  ?s ?p ?o }");

		/* STEP 4 */
		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(
				sparql, set);

		ResultSet results = vqe.execSelect();
		while (results.hasNext()) {
			QuerySolution result = results.nextSolution();
			RDFNode graph = result.get("graph");
			RDFNode s = result.get("s");
			RDFNode p = result.get("p");
			RDFNode o = result.get("o");
			System.out.println(graph + " { " + s + " " + p + " " + o + " . }");
		}

		vqe.close();
	}

	public static void insertDataWithSPARQL() {
		String query = "PREFIX dc: <http://purl.org/dc/elements/1.1/> INSERT { <http://example/egbook> dc:title  \"This is an example title\" } WHERE {}";

		// Create model
		String service = "http://linkeddata.finki.ukim.mk/sparql";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);

		qe.execSelect();

		qe.close();
	}

		
	public static void insertModelToRepository(Model model) {

		String sparqlString = "INSERT INTO GRAPH <" + GRAPH + "> { \n ";

		// list the statements in the Model
		StmtIterator iter = model.listStatements();
		Integer counter = 0;
		// print out the predicate, subject and object of each statement

		while (iter.hasNext()) {
			counter++;

			Statement stmt = iter.nextStatement(); // get next statement
			Resource subject = stmt.getSubject(); // get the subject
			Property predicate = stmt.getPredicate(); // get the predicate
			RDFNode object = stmt.getObject(); // get the object

			sparqlString += "<" + subject.toString() + "> <" + predicate.toString()+ "> <" + object.toString() + "> . \n";
			
			if (counter >= 15) {
				counter = 0;
				// insert model as several smaller sub-models
				sparqlString += "}";
				
				execSPARQLViaHTTPGet(sparqlString);
				
				// reset query for the next sub-model
				sparqlString = "INSERT INTO GRAPH <" + GRAPH + "> { \n ";
			}

		}

		sparqlString += "}";

		execSPARQLViaHTTPGet(sparqlString);
	}
	
	public static String getHTML(String urlToRead) {
		URL url;
		HttpURLConnection conn;
		BufferedReader rd;
		String line;
		String result = "";
		try {
			url = new URL(urlToRead);

			// System.out.println(url);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((line = rd.readLine()) != null) {
				result += line;
			}
			rd.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public static String execSPARQLViaHTTPGet(String sparqlString){
		String httpURL = DATAREPO_SERVICE_ENDPOINT+"?query=";
		System.out.println("SPARQL query");
		System.out.println("**************************************************");
		System.out.println(sparqlString);
		System.out.println("**************************************************");
		httpURL += URLEncoder.encode(sparqlString);
		System.out.println("**************************************************");
		System.out.println(httpURL);
		System.out.println("**************************************************");
	
		return getHTML(httpURL);
	}
	
	public static void clearRepository(){
		String sparqlString = "CLEAR GRAPH <" + GRAPH + ">";
		execSPARQLViaHTTPGet(sparqlString);
		
		System.out.println("Repository cleared.");
	}

	public static void printRepoToFile(String filePath){
		
		Model resourceModel = ModelFactory.createDefaultModel();

		resourceModel.setNsPrefix("datarepo", DATAREPO.ONTOLOGY_URL);
		resourceModel.setNsPrefix("doap", DOAP.getURI());
		resourceModel.setNsPrefix("dct", DCTerms.getURI());
		resourceModel.setNsPrefix("pgo", PGO.ONTOLOGY_URL);
		resourceModel.setNsPrefix("rdf", RDF.getURI());
		resourceModel.setNsPrefix("rdfs", RDFS.getURI());
		
		
		String sparqlString = 
				"SELECT ?s ?p ?o " +
				"FROM <" + DATAREPO.ONTOLOGY_URL + "> \n"+ 
				"WHERE { \n" +
				"?s ?p ?o " +
				" \n}";

		// Create model
		String service = DATAREPO_SERVICE_ENDPOINT;
		QueryExecution qe = QueryExecutionFactory.sparqlService(service,
				sparqlString);

		try {
			ResultSet results = qe.execSelect();
			int counter = 0;
			while (results.hasNext()) {
				counter++;
				if(counter == 3000){
					break;
				}
				QuerySolution soln = results.nextSolution();

				String s = soln.get("s").toString();
				String p = soln.get("p").toString();
				String o = soln.get("o").toString();
				
				Resource res = resourceModel.getResource(s);
				if(res == null){
					res = resourceModel.createResource(s);
				}
				
				Property prop = resourceModel.getProperty(p);
				
				resourceModel.add(res,prop,o);

			}
		} finally {
			qe.close();
		}
		
		
		try {
			Virtuoso.insertModelToRepository(resourceModel);
			
			OutputStreamWriter osw = new OutputStreamWriter(
					new FileOutputStream(filePath));
			resourceModel.write(osw, "RDF/XML-ABBREV");
			// ipCoreModel.write(osw, "TURTLE");
			osw.close();
	
		} catch (FileNotFoundException e) {
			// log.warn(e.getMessage());
		} catch (IOException e) {
			// log.warn(e.getMessage());
		}
	}
	
	public static String executeSPARQLQueryOnModel(String sparqlQuery, Model model){
		String result = new String("");
		
		Query query = QueryFactory.create(sparqlQuery);
		QueryExecution qexec = QueryExecutionFactory.create(query, model);
		ResultSet results = qexec.execSelect();
		for (; results.hasNext();) {
			QuerySolution soln = results.nextSolution();
			result += soln.toString() + "\n";
			System.out.println(soln.toString());
		}
		
		return result;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// ArrayList<Component> comps = listComponents();
		// for (Component component : comps) {
		// System.out.println(component.Name);
		// for(Port p : component.Ports){
		// if(p.Name != null)
		// System.out.println("    "+p.Name+" ");
		// }
		// }

		// getSubClasses("#Architecture");

		// deleteTriplesFromRepository("http://purl.org/net/hdlipcores/ontology/data#");

		// insertModelToRepository(null);

		// System.out.println(countIPCoresPerSourcePortal("http://www.csee.umbc.edu"));

//		clearRepository();
		Model model = ModelFactory.createDefaultModel();
		InputStream in;
		try {
			in = new FileInputStream("/home/zdrave/hdlipcores/web_project/public/files/tmp/1/audio_latest.tar.rdf");
			model.read(in, null);
			insertModelToRepository(model);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // or any windows path

	}

}