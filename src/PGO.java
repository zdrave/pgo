
import java.util.List;

import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

public class PGO {
	//mapping the vhdl.owl URI's
	public static String ONTOLOGY_URL = "http://purl.org/net/hdlipcores/ontology/pgo#";
	//classes
	public static Resource Node 				= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "Node");
	public static Resource Generator 			= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "Generator");
	public static Resource Substation 			= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "Substation");
	public static Resource PowerMeter 			= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "PowerMeter");
	public static Resource Pillar 				= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "Pillar");
	public static Resource TransmissionLine 	= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "TransmissionLine");
	public static Resource Phase 				= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "Phase");
	public static Resource Measurements			= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "Measurements");
	public static Resource Link					= ModelFactory.createDefaultModel().getResource(ONTOLOGY_URL + "Link");
	
	
	//object properties
	//public static Property hasArchitecture	 		= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "hasArchitecture");
	public static Property isConnectedTo	 	= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "isConnectedTo");
	public static Property hasMeasurements		= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "hasMeasurements");
		
	//data properties
	public static Property voltage 				= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "voltage");
	public static Property current		 		= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "current");
	public static Property frequency		 	= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "frequency");
	public static Property activePower		 	= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "activePower");
	public static Property reactivePower		= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "reactivePower");
	public static Property operator				= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "operator");
	public static Property length				= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "length");
	
	
	public static String mapTypeToOntologyType(String type) {
		// get component types from the ontology
		List<String> architectureTypes = Virtuoso.getSubClasses("#Node");

		for (String s : architectureTypes) {
			if (type.toLowerCase().contains(s.toLowerCase())
					|| s.toLowerCase().contains(type.toLowerCase())) {
				return PGO.ONTOLOGY_URL + s;
			}
		}

		return null;
	}
	
}