import com.hp.hpl.jena.rdf.model.Model;

public class Main {

	public static void main(String[] args) {

		Model nodesModel = Annotator.annotateGridKitVertices("/home/zdrave/Downloads/GridKit-conference-de-data-only/gridkit_highvoltage_de_vertices_160727.csv", "/home/zdrave/Downloads/GridKit-conference-de-data-only/gridkit_highvoltage_de_vertices_160727.rdf");

		Model linksModel = Annotator.annotateGridKitLinks("/home/zdrave/Downloads/GridKit-conference-de-data-only/gridkit_highvoltage_de_links_160727.csv", "/home/zdrave/Downloads/GridKit-conference-de-data-only/gridkit_highvoltage_de_links_160727.rdf",nodesModel);

		
		String sparqlQuery = Virtuoso.SPARQL_PREFIXES
				+ "SELECT ?a ?b"
				+ "WHERE { \n"
				+ "?a rdf:type "+"<"+PGO.Generator+"> . "
				+ "?a s:name "+" ?b "
				+ " \n}";
		
		System.out.println(sparqlQuery.toString());
		
		String result1 = Virtuoso.executeSPARQLQueryOnModel(sparqlQuery, linksModel);
		
	}

}
