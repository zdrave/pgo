
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;


import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.DOAP;
import com.hp.hpl.jena.vocabulary.DCTerms;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;


public class Annotator {

	public static Model annotateGridKitVertices(String csvFilePath, String rdfFilePath) {
		Model nodeModel = null;

		nodeModel = ModelFactory.createDefaultModel();
		nodeModel.setNsPrefix("datarepo", DATAREPO.ONTOLOGY_URL);
		nodeModel.setNsPrefix("doap", DOAP.getURI());
		nodeModel.setNsPrefix("dct", DCTerms.getURI());
		nodeModel.setNsPrefix("pgo", PGO.ONTOLOGY_URL);
		nodeModel.setNsPrefix("rdf", RDF.getURI());
		nodeModel.setNsPrefix("rdfs", RDFS.getURI());
		nodeModel.setNsPrefix("s", SCHEMAORG.ONTOLOGY_URL);
		

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		String nodeUri = null;

		try {

			br = new BufferedReader(new FileReader(csvFilePath));
			boolean firstLine = true; //to ignore the header
			int i = 0;
			while ((line = br.readLine()) != null) {
				System.out.print(i++ + ". ");
				if(firstLine){
					firstLine = false;
					continue;
				}
				
				
				
				// use comma as separator
				String[] row = line.split(cvsSplitBy);

				System.out.println(line);
				
				nodeUri = DATAREPO.ONTOLOGY_URL + "node-" + row[0];// +"-" + Utilities.genRandomTimeId();
				
				nodeModel.createResource(nodeUri);
				
				if(row[3].trim().equals("plant")){
					nodeModel.getResource(nodeUri).addProperty(RDF.type,PGO.Generator);
				} else if(row[3].trim().equals("sub_station") || row[3].trim().equals("substation") || row[3].trim().equals("station")){
					nodeModel.getResource(nodeUri).addProperty(RDF.type,PGO.Substation);
				} else if(row[3].trim().equals("joint") || row[3].trim().equals("merge")){ 
					nodeModel.getResource(nodeUri).addProperty(RDF.type,PGO.Pillar);
				} else {
					nodeModel.getResource(nodeUri).addProperty(RDF.type,PGO.Node);	
				}
				
				String geoShapeUri = nodeUri+"-gs";
				String geoCoordinatesUri = geoShapeUri+"-gc";
				
				nodeModel.createResource(geoCoordinatesUri)
					.addProperty(RDF.type, SCHEMAORG.GeoCoordinates)
					.addProperty(SCHEMAORG.longitude, row[1])
					.addProperty(SCHEMAORG.latitude, row[2]);
				
				nodeModel.getResource(nodeUri)
					.addProperty(SCHEMAORG.geo, nodeModel.getResource(geoCoordinatesUri));
				
				if(!row[6].isEmpty()){
					nodeModel.getResource(nodeUri).addProperty(SCHEMAORG.name, row[6]);
				}
				
				if(!row[7].isEmpty()){
					nodeModel.getResource(nodeUri).addProperty(PGO.operator, row[7]);
				}
				
				
				String measurementsUri = nodeUri + "-measurements";
				nodeModel.createResource(measurementsUri)
					.addProperty(RDF.type, PGO.Measurements);
			
				if(!row[4].isEmpty()){
					String splitVoltagesBy = ";";
					String[] voltages = row[4].split(splitVoltagesBy);
					for (String voltage : voltages) {
						nodeModel.getResource(measurementsUri).addProperty(PGO.voltage, voltage);
					}
				}
				
				if(!row[5].isEmpty()){
					String splitFrequencyBy = ";";
					String[] frequencies = row[5].split(splitFrequencyBy);
					for (String frequency : frequencies) {
						nodeModel.getResource(measurementsUri).addProperty(PGO.frequency, frequency);
					}
				}
				
				nodeModel.getResource(nodeUri)
					.addProperty(PGO.hasMeasurements, nodeModel.getResource(measurementsUri));
				
				
				
				
//				Scanner s = new Scanner(System.in);
//				String str = s.nextLine();
				
			}
			
			try {
				//System.out.println(outFilePath);
				OutputStreamWriter osw = new OutputStreamWriter(
						new FileOutputStream(rdfFilePath));
				nodeModel.write(osw, "RDF/XML-ABBREV");
//				ipCoreModel.write(osw, "TURTLE");
				osw.close();
			} catch (FileNotFoundException e) {
//				log.warn(e.getMessage());
			} catch (IOException e) {
//				log.warn(e.getMessage());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return nodeModel;
	}

	public static Model annotateGridKitLinks(String csvFilePath, String rdfFilePath, Model nodesModel) {
		Model linksModel = null;

		linksModel = ModelFactory.createDefaultModel();
		linksModel.setNsPrefix("datarepo", DATAREPO.ONTOLOGY_URL);
		linksModel.setNsPrefix("doap", DOAP.getURI());
		linksModel.setNsPrefix("dct", DCTerms.getURI());
		linksModel.setNsPrefix("pgo", PGO.ONTOLOGY_URL);
		linksModel.setNsPrefix("rdf", RDF.getURI());
		linksModel.setNsPrefix("rdfs", RDFS.getURI());
		linksModel.setNsPrefix("s", SCHEMAORG.ONTOLOGY_URL);
		

		linksModel.add(nodesModel);
		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		String linkUri = null;

		try {

			br = new BufferedReader(new FileReader(csvFilePath));
			boolean firstLine = true; //to ignore the header
			int i = 0;
			while ((line = br.readLine()) != null) {
				System.out.print(i++ + ". ");
				if(firstLine){
					firstLine = false;
					continue;
				}
				
				// use comma as separator
				String[] row = line.split(cvsSplitBy);
				
				System.out.println(line);
				
				linkUri = DATAREPO.ONTOLOGY_URL + "link-" + row[0];// +"-" + Utilities.genRandomTimeId();
				
				linksModel.createResource(linkUri).addProperty(RDF.type, PGO.Link);
				
				
				String node1Uri = DATAREPO.ONTOLOGY_URL + "node-" + row[1];
				String node2Uri = DATAREPO.ONTOLOGY_URL + "node-" + row[2];
				
				if(linksModel.getResource(node1Uri) != null){
					linksModel.getResource(linkUri).addProperty(SCHEMAORG.hasPart, linksModel.getResource(node1Uri));
				}
				
				if(linksModel.getResource(node2Uri) != null){
					linksModel.getResource(linkUri).addProperty(SCHEMAORG.hasPart, linksModel.getResource(node2Uri));
				}
				
				if(!row[3].isEmpty()){
					String splitVoltagesBy = ";";
					String[] voltages = row[3].split(splitVoltagesBy);
					for (String voltage : voltages) {
						linksModel.getResource(linkUri).addProperty(PGO.voltage, voltage);	
					}
				}
				
				if(!row[7].isEmpty()){
					linksModel.getResource(linkUri).addProperty(SCHEMAORG.name, row[7]);
				}
				
				if(!row[8].isEmpty()){
					linksModel.getResource(linkUri).addProperty(PGO.operator, row[8]);
				}
				
				if(!row[8].isEmpty()){
					linksModel.getResource(linkUri).addProperty(PGO.operator, row[8]);
				}
				
				if(!row[10].isEmpty()){
					linksModel.getResource(linkUri).addProperty(PGO.length, row[10]);
				}

			}

			try {
				//System.out.println(outFilePath);
				OutputStreamWriter osw = new OutputStreamWriter(
						new FileOutputStream(rdfFilePath));
				linksModel.write(osw, "RDF/XML-ABBREV");
//				ipCoreModel.write(osw, "TURTLE");
				osw.close();
			} catch (FileNotFoundException e) {
//				log.warn(e.getMessage());
			} catch (IOException e) {
//				log.warn(e.getMessage());
			}

			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return linksModel;
	}

}
