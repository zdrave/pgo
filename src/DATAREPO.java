
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;

public class DATAREPO {
	//mapping the vhdl.owl URI's
	public static String ONTOLOGY_URL = "http://purl.org/net/hdlipcores/ontology/pgo/data#";
		
	//data properties
	public static Property title 		= ModelFactory.createDefaultModel().createProperty(ONTOLOGY_URL + "title");

}